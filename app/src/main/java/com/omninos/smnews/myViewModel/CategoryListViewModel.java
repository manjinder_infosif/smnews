package com.omninos.smnews.myViewModel;

import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.omninos.smnews.Retrofit.Api;
import com.omninos.smnews.Retrofit.ApiClient;
import com.omninos.smnews.Util.CommonUtils;
import com.omninos.smnews.modelClass.CategoryDetailModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryListViewModel extends ViewModel {

    private MutableLiveData<TopNewsModel> categoryDetailModelMutableLiveData;

    public LiveData<TopNewsModel> categoryDetailModelLiveData(Activity activity, Map<String,String> id){
        categoryDetailModelMutableLiveData=new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)){

            Api api= ApiClient.getApiClient().create(Api.class);

            api.getCategory(id).enqueue(new Callback<TopNewsModel>() {
                @Override
                public void onResponse(Call<TopNewsModel> call, Response<TopNewsModel> response) {

                    if (response.isSuccessful()){
                        categoryDetailModelMutableLiveData.setValue(response.body());
                    }else {

                        TopNewsModel categoryDetailModel=new TopNewsModel();
                        categoryDetailModel.setSuccess("0");
                        categoryDetailModel.setMessage("Server Error");
                        categoryDetailModelMutableLiveData.setValue(categoryDetailModel);
                    }
                }

                @Override
                public void onFailure(Call<TopNewsModel> call, Throwable t) {

                    TopNewsModel categoryDetailModel=new TopNewsModel();
                    categoryDetailModel.setSuccess("0");
                    categoryDetailModel.setMessage("Server Error");
                    categoryDetailModelMutableLiveData.setValue(categoryDetailModel);
                }
            });
        }else {

            TopNewsModel categoryDetailModel=new TopNewsModel();
            categoryDetailModel.setSuccess("0");
            categoryDetailModel.setMessage("Internet Error");
            categoryDetailModelMutableLiveData.setValue(categoryDetailModel);
        }
        return categoryDetailModelMutableLiveData;
    }
}
