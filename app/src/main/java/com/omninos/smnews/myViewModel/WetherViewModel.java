package com.omninos.smnews.myViewModel;

import android.app.Activity;
import android.widget.Toast;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.omninos.smnews.Retrofit.Api;
import com.omninos.smnews.Retrofit.ApiClient;
import com.omninos.smnews.Util.CommonUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WetherViewModel extends ViewModel {

    private MutableLiveData<Map> wether;

    public LiveData<Map> getWetherData(Activity activity, String path) {
        if (CommonUtils.isNetworkConnected(activity)) {

            wether = new MutableLiveData<>();
            CommonUtils.showProgress(activity);

            Api api = ApiClient.getApiWether().create(Api.class);

            api.getWether(path).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        wether.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                }
            });
        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return wether;
    }
}
