package com.omninos.smnews.myViewModel;

import android.app.Activity;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.omninos.smnews.Retrofit.Api;
import com.omninos.smnews.Retrofit.ApiClient;
import com.omninos.smnews.Util.CommonUtils;
import com.omninos.smnews.modelClass.SideMenuModel;
import com.omninos.smnews.modelClass.SliderModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryViewModel extends ViewModel {

    private MutableLiveData<SideMenuModel> sideMenuModelMutableLiveData;

    public LiveData<SideMenuModel> sideMenuModelLiveData(Activity activity) {
        sideMenuModelMutableLiveData = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {

            Api api = ApiClient.getApiClient().create(Api.class);
            api.getSide().enqueue(new Callback<SideMenuModel>() {
                @Override
                public void onResponse(Call<SideMenuModel> call, Response<SideMenuModel> response) {

                    if (response.isSuccessful()) {
                        sideMenuModelMutableLiveData.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<SideMenuModel> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return sideMenuModelMutableLiveData;
    }

    private MutableLiveData<TopNewsModel> sliderModelMutableLiveData;

    public LiveData<TopNewsModel> sliderModelLiveData(Activity activity) {
        sliderModelMutableLiveData = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {
            Api api = ApiClient.getApiClient().create(Api.class);
            api.getSlider().enqueue(new Callback<TopNewsModel>() {
                @Override
                public void onResponse(Call<TopNewsModel> call, Response<TopNewsModel> response) {
                    if (response.isSuccessful()) {
                        sliderModelMutableLiveData.setValue(response.body());
                    } else {
                        TopNewsModel sliderModel = new TopNewsModel();
                        sliderModel.setSuccess("0");
                        sliderModel.setMessage("Server Error");
                        sliderModelMutableLiveData.setValue(sliderModel);
                    }
                }

                @Override
                public void onFailure(Call<TopNewsModel> call, Throwable t) {
                    TopNewsModel sliderModel = new TopNewsModel();
                    sliderModel.setSuccess("0");
                    sliderModel.setMessage("Server Error");
                    sliderModelMutableLiveData.setValue(sliderModel);
                }
            });
        } else {
            TopNewsModel sliderModel = new TopNewsModel();
            sliderModel.setSuccess("0");
            sliderModel.setMessage("Internet Error");
            sliderModelMutableLiveData.setValue(sliderModel);
        }
        return sliderModelMutableLiveData;
    }

    private MutableLiveData<TopNewsModel> topnews;

    public LiveData<TopNewsModel> newstop(Activity activity) {
        topnews = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {
            Api api = ApiClient.getApiClient().create(Api.class);
            api.getTopNews().enqueue(new Callback<TopNewsModel>() {
                @Override
                public void onResponse(Call<TopNewsModel> call, Response<TopNewsModel> response) {
                    if (response.isSuccessful()) {
                        topnews.setValue(response.body());
                    } else {
                        TopNewsModel sliderModel = new TopNewsModel();
                        sliderModel.setSuccess("0");
                        sliderModel.setMessage("Server Error");
                        topnews.setValue(sliderModel);
                    }
                }

                @Override
                public void onFailure(Call<TopNewsModel> call, Throwable t) {
                    TopNewsModel sliderModel = new TopNewsModel();
                    sliderModel.setSuccess("0");
                    sliderModel.setMessage("Server Error");
                    topnews.setValue(sliderModel);
                }
            });
        } else {
            TopNewsModel sliderModel = new TopNewsModel();
            sliderModel.setSuccess("0");
            sliderModel.setMessage("Internet Error");
            topnews.setValue(sliderModel);
        }
        return topnews;
    }


}
