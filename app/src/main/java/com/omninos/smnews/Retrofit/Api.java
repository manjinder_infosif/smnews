package com.omninos.smnews.Retrofit;


import com.omninos.smnews.modelClass.CategoryDetailModel;
import com.omninos.smnews.modelClass.SideMenuModel;
import com.omninos.smnews.modelClass.SliderModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface Api {

    @GET("apicategory/?postCategoryList")
    Call<SideMenuModel> getSide();

    @GET("apicategory/?trendingPost")
    Call<TopNewsModel> getSlider();

    @GET("apicategory/?offsetTrendingPost")
    Call<TopNewsModel> getTopNews();

    //&&categoryId={id}
    @GET("apicategory/?postCateogry")
    Call<TopNewsModel> getCategory(@QueryMap Map<String, String> address);

    @GET("f8be257a21c5826a7d7408bb759e5dbc/{Path}")
    Call<Map> getWether(@Path("Path") String path);

}
