package com.omninos.smnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.CategoryDetailModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

    private Context context;
    private List<TopNewsModel.Datum> list;
    Choose choose;


    public interface Choose {
        void Select(int position);

    }

    public CategoryListAdapter(Context context, List<TopNewsModel.Datum> list, Choose choose) {
        this.context = context;
        this.list = list;
        this.choose = choose;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_news_layout_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getImage()).into(holder.newsImg);
        holder.newsTitle.setText(list.get(position).getPostTitle());
        holder.descr.setVisibility(View.VISIBLE);
        holder.descr.setText(list.get(position).getPostContent());

        holder.mainlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choose.Select(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView newsImg;
        private TextView newsTitle, descr;
        private LinearLayout mainlay;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            newsImg = itemView.findViewById(R.id.newsImg);
            newsTitle = itemView.findViewById(R.id.newsTitle);
            descr = itemView.findViewById(R.id.descr);
            mainlay = itemView.findViewById(R.id.mainlay);

        }
    }
}
