package com.omninos.smnews.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.SideMenuModel;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context context;
    private List<SideMenuModel.Datum> list;
    Choose choose;

    int rowIndex = -1;


    public interface Choose {
        void Select(int position);
    }

    public CategoryAdapter(Context context, List<SideMenuModel.Datum> list, Choose choose) {
        this.context = context;
        this.list = list;
        this.choose = choose;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.navi_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.categoryTitle.setText(list.get(position).getName());
        holder.mainlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choose.Select(position);
                rowIndex = position;
                notifyDataSetChanged();
            }
        });

        if (rowIndex == position) {
            holder.categoryTitle.setTextColor(Color.BLACK);
        } else {
            holder.categoryTitle.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryTitle;
        private RelativeLayout mainlay;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryTitle = itemView.findViewById(R.id.categoryTitle);
            mainlay = itemView.findViewById(R.id.mainlay);

        }
    }
}
