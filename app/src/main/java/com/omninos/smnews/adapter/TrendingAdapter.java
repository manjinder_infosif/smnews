package com.omninos.smnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.TopNewsModel;

import java.util.List;

public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.MyViewHolder> {

    Context context;
    private List<TopNewsModel.Datum> data;
    Choose choose;

    public TrendingAdapter(Context context, List<TopNewsModel.Datum> data, Choose choose) {
        this.context = context;
        this.data = data;
        this.choose = choose;
    }

    public interface Choose {
        void Select(int position);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_news_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.newsTitle.setText(data.get(position).getPostTitle());
        Glide.with(context).load(data.get(position).getImage()).into(holder.newsImageData);
        holder.newsImageData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choose.Select(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView newsImageData;
        private TextView newsTitle;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            newsImageData = itemView.findViewById(R.id.newsImageData);
            newsTitle = itemView.findViewById(R.id.newsTitle);

        }
    }
}
