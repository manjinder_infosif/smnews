package com.omninos.smnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.WeatherModel;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.MyViewHolder> {
    Context context;
    List<WeatherModel> list;


    public WeatherAdapter(Context context, List<WeatherModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.weather_item_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.day.setText(getTimeStringFromTimestamp((long) list.get(i).getTimeData(), "EEE"));
        double a=list.get(i).getMin();
        Double b=a-32;
        Double c=b*5/9;
        String r= String.valueOf(c);


        String data=new DecimalFormat("##.#").format(Double.parseDouble(r));
        double ab=list.get(i).getMax();
        Double bb=ab-32;
        Double bc=bb*5/9;
        String br= String.valueOf(bc);
        String data1=new DecimalFormat("##.#").format(Double.parseDouble(br));
        myViewHolder.tempratureData.setText(data+"°C/"+data1+"°C");

        if (list.get(i).getIcon().equalsIgnoreCase("cloudy")){
            Glide.with(context).asGif().load(R.raw.cloud).into(myViewHolder.iconData);
        }else if (list.get(i).getIcon().equalsIgnoreCase("clear-day")){

        }else if (list.get(i).getIcon().equalsIgnoreCase("clear-night")){

        }else if (list.get(i).getIcon().equalsIgnoreCase("rain")){
            Glide.with(context).asGif().load(R.raw.rain).into(myViewHolder.iconData);
        }else if (list.get(i).getIcon().equalsIgnoreCase("fog")){
            Glide.with(context).asGif().load(R.raw.cloud).into(myViewHolder.iconData);
        }else {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tempratureData, day;
        private ImageView iconData;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tempratureData = itemView.findViewById(R.id.tempratureData);
            day = itemView.findViewById(R.id.day);
            iconData=itemView.findViewById(R.id.iconData);
        }
    }

    public static String getTimeStringFromTimestamp(long timestamp, String format) {

        java.util.Date dt = new java.util.Date(timestamp * 1000);

        Timestamp time = new Timestamp(dt.getTime());
//        Logger.debug("timestamp", time + "rime" + dt.getDate() + " inside");
        String str = getMessageTime(time.toString(), format);
//        Logger.debug("timestamp", str + dt.getTime() + " inside");
        return str;
    }

    public static String getMessageTime(String time, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf(time);
        String str = format.format(timestamp);
        return str;
    }
}
