package com.omninos.smnews.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.SliderModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import org.w3c.dom.Text;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {
    Context context;
    List<TopNewsModel.Datum> images;
    Choose choose;

    public interface Choose{
        void Select(int position);
    }

    public ViewPagerAdapter(Context context, List<TopNewsModel.Datum> images, Choose choose) {
        this.context = context;
        this.images = images;
        this.choose = choose;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        //   View itemView = mLayoutInflater.inflate(R.layout.home_pager_item, container, false);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.home_pager_item, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image_slider);
        TextView titleData = (TextView) view.findViewById(R.id.titleData);
        RelativeLayout mainLay = (RelativeLayout) view.findViewById(R.id.mainLay);
        Glide.with(context).load(images.get(position).getImage()).into(imageView);
        titleData.setText(images.get(position).getPostTitle());

        mainLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choose.Select(position);
            }
        });
        container.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
