package com.omninos.smnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.SliderModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import java.util.List;

public class TopNewsAdapter extends RecyclerView.Adapter<TopNewsAdapter.MyViewHolder> {
    Context context;
    private List<TopNewsModel.Datum> datumList;
    Choose choose;

    public interface Choose{
        void Select(int position);
    }

    public TopNewsAdapter(Context context, List<TopNewsModel.Datum> datumList, Choose choose) {
        this.context = context;
        this.datumList = datumList;
        this.choose = choose;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_news_layout_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.newsTitle.setText(datumList.get(position).getPostTitle());
        Glide.with(context).load(datumList.get(position).getImage()).into(holder.newsImg);
        holder.mainlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choose.Select(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView newsImg;
        private TextView newsTitle;
        private LinearLayout mainlay;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            newsImg = itemView.findViewById(R.id.newsImg);
            newsTitle = itemView.findViewById(R.id.newsTitle);
            mainlay=itemView.findViewById(R.id.mainlay);
        }
    }
}
