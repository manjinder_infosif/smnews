package com.omninos.smnews.Util;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class App extends Application {

    private static App instance;
    private Context context;
    private static AppPreferences appPreference;
    private static SingltonPojo sinltonPojo;

    public static App getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = this;
        appPreference = AppPreferences.init(context);
        sinltonPojo = new SingltonPojo();
    }

    public static SingltonPojo getSinltonPojo() {
        return sinltonPojo;
    }

    public static AppPreferences getAppPreference() {
        return appPreference;
    }
}
