package com.omninos.smnews.Util;

public class ConstantData {

    public static final String FILE_NAME = "SmmNews";
    public static final String LOG_IN_DATA = "LoginData";
    public static final String TOKEN = "Token";
    public static String EnableInternet = "1";
    public static final String USER_NAME = "UserName";
    public static final String USER_IMAGE = "UserImage";
    public static final String LOGIN_TYPE = "LoginType";
}
