package com.omninos.smnews.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.omninos.smnews.R;
import com.omninos.smnews.Util.CommonUtils;
import com.omninos.smnews.adapter.CategoryAdapter;
import com.omninos.smnews.adapter.TopNewsAdapter;
import com.omninos.smnews.adapter.TrendingAdapter;
import com.omninos.smnews.modelClass.SideMenuModel;
import com.omninos.smnews.modelClass.TopNewsModel;
import com.omninos.smnews.myViewModel.CategoryListViewModel;
import com.omninos.smnews.myViewModel.CategoryViewModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView homeButton;

    private RelativeLayout content;
    private DrawerLayout drawerLayout;
    private float slideX;
    //    private ViewPager image_slider;
//    private CircleIndicator indicator;
    private int offerSize;
    private SwipeRefreshLayout swipe_layout;


    private RecyclerView topNewsRecyclerView, mainData, trendingNewsRecyclerView;

    //adapter
    CategoryAdapter adapter;
    TopNewsAdapter topNewsAdapter;
    TrendingAdapter trendingAdapter;


    private Timer timer;
    protected boolean isDone = false;
    private LinearLayout slider;
    private ImageView tv_icon, menu, radio_icon, pro_pic;


    //viewModel
    CategoryViewModel viewModel;
    CategoryListViewModel viewModel1;

    //adMob
    private static final String AD_UNIT_ID = "ca-app-pub-6134124463237490/6717430320";
    private InterstitialAd interstitialAd;

    private Timer myTimer;

    int[] animationList = {R.anim.layout_animation_up_to_down, R.anim.layout_animation_right_to_left, R.anim.item_animation_fall_down, R.anim.layout_animation_left_to_right};


    //side navi
    private CardView weatherCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        viewModel1 = ViewModelProviders.of(this).get(CategoryListViewModel.class);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(AD_UNIT_ID);
        initView();
        SetUp();


        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
//                Toast.makeText(MainActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
//                Toast.makeText(MainActivity.this, "Pass", Toast.LENGTH_SHORT).show();
            }
        });
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showInterstitial();
                    }
                });

            }

        }, 100000, 100000);


    }

    private void showInterstitial() {
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                AdRequest adRequest = new AdRequest.Builder().build();
                interstitialAd.loadAd(adRequest);
            }
        }
    }


    private void initView() {


        homeButton = findViewById(R.id.homeButton);
        swipe_layout = findViewById(R.id.swipe_layout);

        content = findViewById(R.id.content);

        slider = findViewById(R.id.slider);

        tv_icon = findViewById(R.id.tv_icon);
        menu = findViewById(R.id.menu);
        radio_icon = findViewById(R.id.radio_icon);

        topNewsRecyclerView = findViewById(R.id.topNewsRecyclerView);
        trendingNewsRecyclerView = findViewById(R.id.trendingNewsRecyclerView);

        mainData = findViewById(R.id.mainData);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        pro_pic = findViewById(R.id.pro_pic);

        weatherCard = findViewById(R.id.weatherCard);
        weatherCard.setOnClickListener(this);

        Glide.with(MainActivity.this).load(R.raw.logo_img).into(pro_pic);

    }

    private void SetUp() {
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this);
        linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        topNewsRecyclerView.setLayoutManager(linearLayoutManager1);

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
        mainData.setLayoutManager(linearLayoutManager2);

        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(this);
        linearLayoutManager3.setOrientation(LinearLayoutManager.HORIZONTAL);
        trendingNewsRecyclerView.setLayoutManager(linearLayoutManager3);

        menu.setOnClickListener(this);
        tv_icon.setOnClickListener(this);

        drawerLayout.setScrimColor(Color.TRANSPARENT);

        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                slideX = drawerView.getWidth() * slideOffset;
                content.setTranslationX(slideX);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);


        swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSidemenu();

            }
        });

        swipe_layout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark, R.color.black, R.color.yellow);

        swipe_layout.post(new Runnable() {
            @Override
            public void run() {
                swipe_layout.setRefreshing(true);
                getSidemenu();

            }
        });


    }

    private void getTopNews() {


        viewModel.newstop(MainActivity.this).observe(MainActivity.this, new Observer<TopNewsModel>() {
            @Override
            public void onChanged(final TopNewsModel sliderModel) {
                if (sliderModel.getSuccess().equalsIgnoreCase("1")) {
                    topNewsAdapter = new TopNewsAdapter(MainActivity.this, sliderModel.getData(), new TopNewsAdapter.Choose() {
                        @Override
                        public void Select(int position) {
                            startActivity(new Intent(MainActivity.this, NewsDetailActivity.class).putExtra("Details", sliderModel.getData().get(position)).putExtra("data", "2").putExtra("Name", "Trending"));
                        }
                    });

                    final int min = 0;
                    final int max = 3;
                    final int random = new Random().nextInt((max - min) + 1) + min;
                    System.out.println("Data: " + random);


                    final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(MainActivity.this, animationList[random]);

                    topNewsRecyclerView.setLayoutAnimation(controller);
                    topNewsAdapter.notifyDataSetChanged();
                    topNewsRecyclerView.scheduleLayoutAnimation();
                    topNewsRecyclerView.setAdapter(topNewsAdapter);
                } else {
                    CommonUtils.showSnackbarAlert(topNewsRecyclerView, sliderModel.getMessage());
                }
            }
        });

    }

    private void getSlider() {
        slider.setVisibility(View.VISIBLE);
        if (isDone) {
            timer.cancel();
        }
        viewModel.sliderModelLiveData(MainActivity.this).observe(MainActivity.this, new Observer<TopNewsModel>() {
            @Override
            public void onChanged(final TopNewsModel sliderModel) {
                if (sliderModel.getSuccess().equalsIgnoreCase("1")) {


                    trendingAdapter = new TrendingAdapter(MainActivity.this, sliderModel.getData(), new TrendingAdapter.Choose() {
                        @Override
                        public void Select(int position) {
                            startActivity(new Intent(MainActivity.this, NewsDetailActivity.class).putExtra("Details", sliderModel.getData().get(position)).putExtra("data", "2").putExtra("Name", "Top News"));
                        }
                    });
                    trendingNewsRecyclerView.setAdapter(trendingAdapter);

//                    offerSize = sliderModel.getData().size();
//
//                    image_slider.setAdapter(new ViewPagerAdapter(MainActivity.this, sliderModel.getData(), new ViewPagerAdapter.Choose() {
//                        @Override
//                        public void Select(int position) {
//                            startActivity(new Intent(MainActivity.this, NewsDetailActivity.class).putExtra("Details", sliderModel.getData().get(position)).putExtra("data", "2"));
//                        }
//                    }));
//                    indicator.setViewPager(image_slider);
//
//                    TimerTask timerTask = new TimerTask() {
//                        @Override
//                        public void run() {
//                            image_slider.post(new Runnable() {
//
//                                @Override
//                                public void run() {
//                                    isDone = true;
//                                    if (image_slider.getCurrentItem() == offerSize - 1) {
//                                        image_slider.setCurrentItem(0);
//                                    } else {
//                                        image_slider.setCurrentItem(image_slider.getCurrentItem() + 1);
//                                    }
//                                }
//                            });
//                        }
//                    };
//                    timer = new Timer();
//                    timer.schedule(timerTask, 3000, 3000);


                } else {
                    CommonUtils.showSnackbarAlert(topNewsRecyclerView, sliderModel.getMessage());
                }
            }
        });

        getTopNews();
    }

    private void getSidemenu() {
        viewModel.sideMenuModelLiveData(MainActivity.this).observe(MainActivity.this, new Observer<SideMenuModel>() {
            @Override
            public void onChanged(final SideMenuModel sideMenuModel) {
                swipe_layout.setRefreshing(false);
                adapter = new CategoryAdapter(MainActivity.this, sideMenuModel.getData(), new CategoryAdapter.Choose() {
                    @Override
                    public void Select(int position) {
                        getCategoryNews(String.valueOf(sideMenuModel.getData().get(position).getCatID()), sideMenuModel.getData().get(position).getName());
                    }
                });
                mainData.setAdapter(adapter);
            }
        });
        getSlider();
    }

    private void getCategoryNews(String id, final String name) {
        swipe_layout.setRefreshing(true);
        Map<String, String> data = new HashMap<>();
        data.put("categoryId", id);
        viewModel1.categoryDetailModelLiveData(MainActivity.this, data).observe(MainActivity.this, new Observer<TopNewsModel>() {
            @Override
            public void onChanged(final TopNewsModel categoryDetailModel) {
                swipe_layout.setRefreshing(false);
                if (categoryDetailModel.getSuccess().equalsIgnoreCase("1")) {
                    topNewsAdapter = new TopNewsAdapter(MainActivity.this, categoryDetailModel.getData(), new TopNewsAdapter.Choose() {
                        @Override
                        public void Select(int position) {
                            startActivity(new Intent(MainActivity.this, NewsDetailActivity.class).putExtra("Details", categoryDetailModel.getData().get(position)).putExtra("data", "2").putExtra("Name", name));
                        }
                    });

                    final int min = 0;
                    final int max = 3;
                    final int random = new Random().nextInt((max - min) + 1) + min;
                    System.out.println("Data: " + random);
                    final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(MainActivity.this, animationList[random]);
                    topNewsRecyclerView.setLayoutAnimation(controller);
                    topNewsAdapter.notifyDataSetChanged();
                    topNewsRecyclerView.scheduleLayoutAnimation();
                    topNewsRecyclerView.setAdapter(topNewsAdapter);
                    slider.setVisibility(View.GONE);

                } else {
                    CommonUtils.showSnackbarAlert(topNewsRecyclerView, categoryDetailModel.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.tv_icon:
                Youtube();
                break;
            case R.id.weatherCard:
                startActivity(new Intent(MainActivity.this, WeatherActivity.class));
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myTimer.cancel();
    }

    private void Youtube() {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCPN0scogx1HUpOTEgwDeJ0Q"));
        try {
            startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
        }
    }
}
