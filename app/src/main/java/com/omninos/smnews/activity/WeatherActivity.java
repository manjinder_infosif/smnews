package com.omninos.smnews.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.internal.LinkedTreeMap;
import com.omninos.smnews.R;
import com.omninos.smnews.Util.App;
import com.omninos.smnews.adapter.WeatherAdapter;
import com.omninos.smnews.modelClass.WeatherModel;
import com.omninos.smnews.myViewModel.WetherViewModel;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener, Animation.AnimationListener, LocationListener {

    private RecyclerView recycler_view;
    private WeatherAdapter adapter;
    private ImageView SunImage;
    private TextView title, addressName, wetherType, temprature;

    Animation animFadein;

    private RelativeLayout enableGPS,main;


    private List<WeatherModel> list = new ArrayList<>();

    //viewModel
    WetherViewModel viewModel;

    LocationManager locationManager;
    boolean isDone = false;

    Button button;
    Context context;
    Intent intent1;
    TextView textview;
    boolean GpsStatus ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        viewModel = ViewModelProviders.of(this).get(WetherViewModel.class);
        button = (Button)findViewById(R.id.button1);
        textview = (TextView)findViewById(R.id.textView1);
        context = getApplicationContext();


        initView();
        SetUps();


        animFadein = AnimationUtils.loadAnimation(this,
                R.anim.rotate_clockwise);

        animFadein.setAnimationListener(this);




        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent1);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckGpsStatus();
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(GpsStatus == true) {
            textview.setText("GPS Is Enabled");
            getLocation();
            enableGPS.setVisibility(View.GONE);
            main.setVisibility(View.VISIBLE);
        } else {
            textview.setText("GPS Is Disabled");
            enableGPS.setVisibility(View.VISIBLE);
            main.setVisibility(View.GONE);
        }
    }


    private void getData(final String lat, final String lng) {

        viewModel.getWetherData(WeatherActivity.this, lat + "," + lng).observe(WeatherActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                for (int i = 0; i < ((ArrayList) ((LinkedTreeMap) map.get("daily")).get("data")).size(); i++) {
                    WeatherModel weatherModel = new WeatherModel((double) ((LinkedTreeMap) ((ArrayList) ((LinkedTreeMap) map.get("daily")).get("data")).get(i)).get("time"), ((LinkedTreeMap) ((ArrayList) ((LinkedTreeMap) map.get("daily")).get("data")).get(i)).get("icon").toString(), (double) ((LinkedTreeMap) ((ArrayList) ((LinkedTreeMap) map.get("daily")).get("data")).get(i)).get("temperatureHigh"), (double) ((LinkedTreeMap) ((ArrayList) ((LinkedTreeMap) map.get("daily")).get("data")).get(i)).get("temperatureLow"));
                    list.add(weatherModel);
                }
                getAddress(lat,lng);
                getTemp((double) ((LinkedTreeMap) map.get("currently")).get("temperature"));
                wetherType.setText(((LinkedTreeMap) map.get("currently")).get("summary").toString());
                if (((LinkedTreeMap) map.get("currently")).get("icon").toString().equalsIgnoreCase("cloudy")) {
                    Glide.with(WeatherActivity.this).asGif().load(R.raw.cloud).into(SunImage);
                } else if (((LinkedTreeMap) map.get("currently")).get("icon").toString().equalsIgnoreCase("clear-day")) {
                    SunImage.startAnimation(animFadein);
                } else if (((LinkedTreeMap) map.get("currently")).get("icon").toString().equalsIgnoreCase("clear-night")) {
                    SunImage.startAnimation(animFadein);
                } else if (((LinkedTreeMap) map.get("currently")).get("icon").toString().equalsIgnoreCase("rain")) {
                    Glide.with(WeatherActivity.this).asGif().load(R.raw.rain).into(SunImage);
                } else if (((LinkedTreeMap) map.get("currently")).get("icon").toString().equalsIgnoreCase("fog")) {
                    Glide.with(WeatherActivity.this).asGif().load(R.raw.cloud).into(SunImage);
                } else {
                    SunImage.startAnimation(animFadein);
                }

                adapter = new WeatherAdapter(WeatherActivity.this, list);
                recycler_view.setAdapter(adapter);
            }
        });
    }

    private void getTemp(double v) {
        double ab = v;
        Double bb = ab - 32;
        Double bc = bb * 5 / 9;
        String br = String.valueOf(bc);
        String data1 = new DecimalFormat("##").format(Double.parseDouble(br));
        temprature.setText(data1 + "°C");
    }


    private void getAddress(String lat, String lng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lng), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            addressName.setText(city);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void initView() {
        recycler_view = findViewById(R.id.recycler_view);
//        back = findViewById(R.id.back);
        title = findViewById(R.id.mainTitle);
        SunImage = findViewById(R.id.SunImage);
        addressName = findViewById(R.id.addressName);
        wetherType = findViewById(R.id.wetherType);

        temprature = findViewById(R.id.temprature);
        enableGPS=findViewById(R.id.enableGPS);
        main=findViewById(R.id.main);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_view.setLayoutManager(linearLayoutManager);


    }

    private void SetUps() {

//        back.setImageDrawable(getDrawable(R.drawable.ic_back));
//        back.setOnClickListener(this);
        title.setText("Weather");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.back:
//                onBackPressed();
//                break;
        }
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (!isDone) {
            getData(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
            isDone = true;

        }

//        addressName.setText("Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude());
//
//        try {
//            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
//            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//            addressName.setText(addressName.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
//                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
//        }catch(Exception e)
//        {
//
//        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(WeatherActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }
}
