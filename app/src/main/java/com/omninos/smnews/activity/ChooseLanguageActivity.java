package com.omninos.smnews.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.omninos.smnews.R;

public class ChooseLanguageActivity extends AppCompatActivity implements View.OnClickListener {

    private Button getStartButton;
    private CardView cardPunjabi, cardEnglish, cardHindi;
    private TextView englishText, punjabiText, hindiText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);

        initView();
        SetUps();

    }

    private void SetUps() {
        getStartButton.setOnClickListener(this);
        cardPunjabi.setOnClickListener(this);
        cardEnglish.setOnClickListener(this);
        cardHindi.setOnClickListener(this);
    }

    private void initView() {
        getStartButton = findViewById(R.id.getStartButton);
        cardPunjabi = findViewById(R.id.cardPunjabi);
        cardEnglish = findViewById(R.id.cardEnglish);
        cardHindi = findViewById(R.id.cardHindi);

        englishText = findViewById(R.id.englishText);
        punjabiText = findViewById(R.id.punjabiText);
        hindiText = findViewById(R.id.hindiText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getStartButton:
                startActivity(new Intent(ChooseLanguageActivity.this, MainActivity.class));
                finishAffinity();
                break;

            case R.id.cardEnglish:
                englishText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_selected, 0, 0, 0);
                englishText.setTextColor(getResources().getColor(R.color.black));

                punjabiText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_unselected, 0, 0, 0);
                punjabiText.setTextColor(getResources().getColor(R.color.gray));

                hindiText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_unselected, 0, 0, 0);
                hindiText.setTextColor(getResources().getColor(R.color.gray));
                break;

            case R.id.cardPunjabi:
                englishText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_unselected, 0, 0, 0);
                englishText.setTextColor(getResources().getColor(R.color.gray));

                punjabiText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_selected, 0, 0, 0);
                punjabiText.setTextColor(getResources().getColor(R.color.black));

                hindiText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_unselected, 0, 0, 0);
                hindiText.setTextColor(getResources().getColor(R.color.gray));
                break;
            case R.id.cardHindi:
                englishText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_unselected, 0, 0, 0);
                englishText.setTextColor(getResources().getColor(R.color.gray));

                punjabiText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_unselected, 0, 0, 0);
                punjabiText.setTextColor(getResources().getColor(R.color.gray));

                hindiText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_selected, 0, 0, 0);
                hindiText.setTextColor(getResources().getColor(R.color.black));
                break;
        }
    }
}
