package com.omninos.smnews.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.smnews.R;
import com.omninos.smnews.Util.CommonUtils;
import com.omninos.smnews.adapter.CategoryListAdapter;
import com.omninos.smnews.modelClass.CategoryDetailModel;
import com.omninos.smnews.modelClass.TopNewsModel;
import com.omninos.smnews.myViewModel.CategoryListViewModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CategoryListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CategoryListAdapter adapter;
    private CategoryListViewModel viewModel;
    private String id,title;
    private ImageView homeButton;
    private TextView mainTitle;
    private SwipeRefreshLayout swipeRefreshLayout;
    int[] animationList = {R.anim.layout_animation_up_to_down, R.anim.layout_animation_right_to_left, R.anim.item_animation_fall_down, R.anim.layout_animation_left_to_right};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        viewModel = ViewModelProviders.of(this).get(CategoryListViewModel.class);
        id=getIntent().getStringExtra("id");
        title=getIntent().getStringExtra("title");
        initView();
        SetUp();
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        homeButton=findViewById(R.id.homeButton);

        mainTitle=findViewById(R.id.mainTitle);
        swipeRefreshLayout=findViewById(R.id.swipeRefreshLayout);


    }

    private void SetUp() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        mainTitle.setText(title);

        homeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getList();
            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark, R.color.black, R.color.yellow);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                getList();
            }
        });

    }

    private void getList() {
        Map<String, String> data = new HashMap<>();
        data.put("categoryId", id);
        viewModel.categoryDetailModelLiveData(CategoryListActivity.this, data).observe(CategoryListActivity.this, new Observer<TopNewsModel>() {
            @Override
            public void onChanged(final TopNewsModel categoryDetailModel) {
                swipeRefreshLayout.setRefreshing(false);
                if (categoryDetailModel.getSuccess().equalsIgnoreCase("1")) {
                    adapter = new CategoryListAdapter(CategoryListActivity.this, categoryDetailModel.getData(), new CategoryListAdapter.Choose() {
                        @Override
                        public void Select(int position) {
                            startActivity(new Intent(CategoryListActivity.this,NewsDetailActivity.class).putExtra("Details",categoryDetailModel.getData().get(position)).putExtra("data","2").putExtra("Name","Trending"));
                        }
                    });

                    final int min = 0;
                    final int max = 3;
                    final int random = new Random().nextInt((max - min) + 1) + min;
                    System.out.println("Data: " + random);
                    final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(CategoryListActivity.this, animationList[random]);
                    recyclerView.setLayoutAnimation(controller);
                    adapter.notifyDataSetChanged();
                    recyclerView.scheduleLayoutAnimation();
                    recyclerView.setAdapter(adapter);
                } else {
                    CommonUtils.showSnackbarAlert(recyclerView, categoryDetailModel.getMessage());
                }
            }
        });
    }
}
