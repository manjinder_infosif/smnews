package com.omninos.smnews.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.omninos.smnews.R;
import com.omninos.smnews.modelClass.CategoryDetailModel;
import com.omninos.smnews.modelClass.TopNewsModel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class NewsDetailActivity extends AppCompatActivity {

    private RoundedImageView newsimge;
    private TextView titleData, descr, mainTitle, dateAndTime;
    private TopNewsModel.Datum Details;
    private CategoryDetailModel.Datum categoryDetailModel;
    private ImageView homeButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        if (getIntent().getStringExtra("data").equalsIgnoreCase("2")) {
            Details = (TopNewsModel.Datum) getIntent().getSerializableExtra("Details");
        } else if (getIntent().getStringExtra("data").equalsIgnoreCase("1")) {
            categoryDetailModel = (CategoryDetailModel.Datum) getIntent().getSerializableExtra("Details");
        }
        initView();
        SetUp();
    }

    private void initView() {
        newsimge = findViewById(R.id.newsimge);
        titleData = findViewById(R.id.titleData);
        descr = findViewById(R.id.descr);
        homeButton = findViewById(R.id.homeButton);
        mainTitle = findViewById(R.id.mainTitle);
        dateAndTime = findViewById(R.id.dateAndTime);
    }

    private void SetUp() {
//        if (getIntent().getStringExtra("Name")==null) {
//            mainTitle.setText("Trending");
//        } else {
            mainTitle.setText(getIntent().getStringExtra("Name"));
//        }
        homeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (getIntent().getStringExtra("data").equalsIgnoreCase("2")) {
            Glide.with(NewsDetailActivity.this).load(Details.getImage()).into(newsimge);
            titleData.setText(Details.getPostTitle());
            descr.setText(Details.getPostContent().replace("&nbsp;", ""));
            String data = Details.getPostContent().replace("&nbsp;", "");
            System.out.println("Data: " + data.indexOf("<a href=\""));
            System.out.println("Data: " + data.substring(data.indexOf("<a href=\"") + 9));
            int counter = data.split("<a href=\"", -1).length - 1;
            System.out.println("Data: " + counter);
            dateAndTime.setText(Details.getPostDate());
        } else {
            Glide.with(NewsDetailActivity.this).load(categoryDetailModel.getImage()).into(newsimge);
            titleData.setText(categoryDetailModel.getTitle());
            descr.setText(categoryDetailModel.getPostContent().replace("&nbsp;", ""));
            dateAndTime.setText(categoryDetailModel.getDate());
        }
    }
}
