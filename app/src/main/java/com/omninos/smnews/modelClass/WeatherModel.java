package com.omninos.smnews.modelClass;

public class WeatherModel {
    private double timeData,max,min;
    private String icon;

    public WeatherModel(double timeData, String icon, double max, double min) {
        this.timeData = timeData;
        this.icon = icon;
        this.max = max;
        this.min = min;
    }

    public double getTimeData() {
        return timeData;
    }

    public void setTimeData(double timeData) {
        this.timeData = timeData;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
